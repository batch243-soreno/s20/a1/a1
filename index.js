
let num = Number(prompt("Enter a number:"));
console.log("The number you provided is, " + num);
  for (let i = num; i >= 0; i -= 5) {
  // console.log(i);
    if (i <= 50) {
      console.log("The current value is at " + (i) + ". Terminating the loop.");
      break;
  }

    if (i % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
      continue;
    }

    if (i % 5 === 0) {
      console.log(i);
    }
}

let str = "supercalifragilisticexpialidocious";
let consonant = "";
  for (let i = 0; i < str.length; i++) {
    if (
      str[i].toLowerCase() === "a" ||
      str[i].toLowerCase() === "e" ||
      str[i].toLowerCase() === "i" ||
      str[i].toLowerCase() === "o" ||
      str[i].toLowerCase() === "u"
    )
      continue;
    else {
      consonant += str[i];
    }
  }

console.log(consonant);
